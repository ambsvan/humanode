#Humanode - In Development ** Not ready for running **

## Personal Health Information System

[Install Node](http://nodejs.org/download/)

[Trello Board](https://trello.com/b/euNSHihh/humanode) - used for build planning

##In your command line:

- Make a new directory: mkdir Humanode
- Change into new directory: cd Humanode
- Clone this repo in your command line: git clone your_repo_clone_url
- Install Dependencies: npm install 
- Start the server: grunt

##In your browser:
- Open up http://localhost:3000/
- Sign up with a new account