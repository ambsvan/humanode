// load all the things we need
// require('../.env')
var LocalStrategy       = require('passport-local').Strategy;
var HumanApiStrategy    = require('passport-humanapi').Strategy;
var findOneOrCreate     = require('mongoose-find-one-or-create');
// load up the user model
var User            = require('../app/models/user');

var HUMANAPI_APP_ID ='6369f3d888308cb48bdcf218795669a7ab8e47f2';
var HUMANAPI_APP_SECRET ='846ab8828646c2de1d3df970f13a8407c35b543b';

// expose this function to our app using module.exports
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) {

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);

            // check to see if theres already a user with that email
            if (user) {
                return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

                // if there is no user with that email
                // create the user
                var newUser            = new User();

                // set the user's local credentials
                newUser.local.email    = email;
                newUser.local.password = newUser.generateHash(password); // use the generateHash function in our user model
                console.log("Saving user");
                // save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }

        });

    }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user)
                return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

            // if the user is found but the password is wrong
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

            // all is well, return successful user
            return done(null, user);
        });

    }));

    // =========================================================================
    // HUMAN API STRATEGY CONFIGURATION - Saves access token to DB
    // =========================================================================

    passport.use('humanapi', new HumanApiStrategy({
        authorizationURL: "https://user.humanapi.co/oauth2/authorize",
        clientID : HUMANAPI_APP_ID,
        clientSecret : HUMANAPI_APP_SECRET,
        callbackURL: "http://localhost:3000/auth/humanapi/callback",
        tokenURL: "https://user.humanapi.co/v1/connect/tokens"
    },

    function(accessToken, refreshToken, profile, done) {

        process.nextTick(function () {
        
            User.findOneOrCreate({ userId: profile.id }, function (err, user) {

                if (err)
                return done(err);

                else {

                    user.accessToken       = accessToken;
                    user.refreshToken      = refreshToken;
                    profile.publicToken    = publicToken;
                    profile.humanID        = humanID; 

                    console.log("Saving tokens");
                    // save the user
                    user.save(function(err) {
                        if (err)
                            throw err;
                            
                      return done(err, user);
                    
                    });
                }
            });        
        });   
            
    }));




};